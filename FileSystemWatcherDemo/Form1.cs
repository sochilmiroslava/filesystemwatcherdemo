﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FileSystemWatcherDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

			//this.ShowInTaskbar = false;
			//this.FormBorderStyle = FormBorderStyle.FixedToolWindow;

			//NotifyIcon notifyIcon = new NotifyIcon();
			//notifyIcon.Visible = true;

			
			//	notifyIcon.BalloonTipTitle = "Whatcher";
			

			//notifyIcon.ShowBalloonTip(30000);
		}


        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void fsw_changed(object sender, FileSystemEventArgs e)
        {
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Changed:
                    lbxCambios.Items.Add("Archivo Cambiado: ");
					//Uri Direccion2 = new Uri("http://neapco.transferspc.com/manage/TestingUpload");
					Uri Direccion2 = new Uri(urlTexbox.Text);
					webBrowser1.Url = Direccion2;
					break;
                case WatcherChangeTypes.Created:
                    lbxCambios.Items.Add("Archivo Creado: Aqui hay que procesar");
					System.Threading.Thread.Sleep(2000);
					//Uri Direccion = new Uri("http://neapco.transferspc.com/manage/TestingUpload");
					Uri Direccion = new Uri(urlTexbox.Text);
					
					webBrowser1.Url = Direccion;
					break;
                case WatcherChangeTypes.Deleted:
                    lbxCambios.Items.Add("Archivo Eliminado: ");
                    break;
               
            }
            
        }
        private void fsw_renamed(object sender, RenamedEventArgs e)
        {
            lbxCambios.Items.Add("Archivo renombrado de " + e.OldName + " a " + e.Name);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //chknotificaciones.Items.Add(NotifyFilters.Attributes, true);
            //chknotificaciones.Items.Add(NotifyFilters.CreationTime, true);
            //chknotificaciones.Items.Add(NotifyFilters.DirectoryName, true);
            //chknotificaciones.Items.Add(NotifyFilters.FileName, true);
            //chknotificaciones.Items.Add(NotifyFilters.LastAccess, true);
            //chknotificaciones.Items.Add(NotifyFilters.LastWrite, true);
            //chknotificaciones.Items.Add(NotifyFilters.Security, true);
            //chknotificaciones.Items.Add(NotifyFilters.Size, true);
        }

        private bool ValidarRuta()
        {
            bool bStatus = true;
            if (txtRuta.Text == "")
            {
                errorProvider1.SetError(txtRuta, "Por favor Seleccione una ruta válida");
                bStatus = false;
            }
            else
            {
                DirectoryInfo dir = new DirectoryInfo(txtRuta.Text);
                if (dir.Exists)
                {
                    errorProvider1.SetError(txtRuta, "");
                    // validar el texto del filtro
                    if (!String.IsNullOrWhiteSpace(txtFiltro.Text))
                    {
                        errorProvider1.SetError(txtFiltro, "");
                        //CheckedListBox.CheckedIndexCollection cont = chknotificaciones.CheckedIndices;
                        //if (cont.Count > 0)
                        //{
                        //    errorProvider1.SetError(chknotificaciones, "");
                        //}
                        //else
                        //{
                        //    errorProvider1.SetError(chknotificaciones, "Por favor Seleccione al menos una notificación");
                        //}
                        //if(chknotificaciones.CheckedIndices)
                    }
                    else
                    {
                        errorProvider1.SetError(txtFiltro, "Por favor Seleccione un filtro válido");
                        bStatus = false;
                    }
                }
                else
                {
                    
                    errorProvider1.SetError(txtRuta, "Por favor Seleccione una ruta válida");
                    bStatus = false;
                }
            }
            return bStatus;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            if (ValidarRuta())
            {
                if (btnIniciar.Text == "Iniciar")
                {

                    btnIniciar.Text = "Detener";
                    fileSystemWatcher1.Path = txtRuta.Text;

                    fileSystemWatcher1.Changed += new FileSystemEventHandler(fsw_changed);
                    fileSystemWatcher1.Created += new FileSystemEventHandler(fsw_changed);
                    fileSystemWatcher1.Deleted += new FileSystemEventHandler(fsw_changed);
                    fileSystemWatcher1.Renamed += new RenamedEventHandler(fsw_renamed);
                    if (subSi.Checked == true)
                    {
                        fileSystemWatcher1.IncludeSubdirectories = true;
                    }
                    if (subNo.Checked == true)
                    {
                        fileSystemWatcher1.IncludeSubdirectories = false;
                    }
                    fileSystemWatcher1.Filter = txtFiltro.Text;
                    fileSystemWatcher1.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.Attributes 
                        | NotifyFilters.CreationTime | NotifyFilters.DirectoryName | NotifyFilters.FileName
                        | NotifyFilters.LastWrite | NotifyFilters.Security | NotifyFilters.Size;
                    //foreach (object itemcheck in chknotificaciones.CheckedItems)
                    //{
                    //    fileSystemWatcher1.NotifyFilter = (NotifyFilters)itemcheck;
                    //}
                    fileSystemWatcher1.EnableRaisingEvents = true;

                }
                else
                {
                    btnIniciar.Text = "Iniciar";
                    fileSystemWatcher1.EnableRaisingEvents = false;
                }
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtRuta.Text = folderBrowserDialog1.SelectedPath;
                DirectoryInfo dir = new DirectoryInfo(txtRuta.Text);
                foreach (FileInfo fi in dir.GetFiles())
                {
                    lbxArchivos.Items.Add(fi.Name);
                }

            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

		private void Button1_Click_2(object sender, EventArgs e)
		{
			Uri Direccion = new Uri("http://google.com");
			webBrowser1.Url = Direccion;
		}

		private void Button2_Click_1(object sender, EventArgs e)
		{
			Uri Direccion = new Uri(urlTexbox.Text);
			//Uri Direccion = new Uri("http://neapco.transferspc.com/manage/TestingUpload");
			webBrowser1.Url = Direccion;
		}

		private void Button3_Click(object sender, EventArgs e)
		{
			lbxArchivos.Items.Clear();
			lbxCambios.Items.Clear();
		
		}
	}
}
