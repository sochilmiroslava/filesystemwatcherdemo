﻿namespace FileSystemWatcherDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.lbxArchivos = new System.Windows.Forms.ListBox();
			this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.label1 = new System.Windows.Forms.Label();
			this.lbxCambios = new System.Windows.Forms.ListBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.txtFiltro = new System.Windows.Forms.TextBox();
			this.subNo = new System.Windows.Forms.RadioButton();
			this.subSi = new System.Windows.Forms.RadioButton();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btnIniciar = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.txtRuta = new System.Windows.Forms.TextBox();
			this.btnBuscar = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.webBrowser1 = new System.Windows.Forms.WebBrowser();
			this.button3 = new System.Windows.Forms.Button();
			this.urlTexbox = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// lbxArchivos
			// 
			this.lbxArchivos.FormattingEnabled = true;
			this.lbxArchivos.ItemHeight = 20;
			this.lbxArchivos.Location = new System.Drawing.Point(20, 227);
			this.lbxArchivos.Name = "lbxArchivos";
			this.lbxArchivos.Size = new System.Drawing.Size(542, 104);
			this.lbxArchivos.TabIndex = 2;
			// 
			// fileSystemWatcher1
			// 
			this.fileSystemWatcher1.EnableRaisingEvents = true;
			this.fileSystemWatcher1.SynchronizingObject = this;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(17, 208);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(79, 20);
			this.label1.TabIndex = 3;
			this.label1.Text = "Archivos:";
			// 
			// lbxCambios
			// 
			this.lbxCambios.FormattingEnabled = true;
			this.lbxCambios.ItemHeight = 20;
			this.lbxCambios.Location = new System.Drawing.Point(20, 367);
			this.lbxCambios.Name = "lbxCambios";
			this.lbxCambios.Size = new System.Drawing.Size(686, 104);
			this.lbxCambios.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(17, 346);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(168, 20);
			this.label2.TabIndex = 5;
			this.label2.Text = "Cambios Realizados:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.urlTexbox);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.txtFiltro);
			this.groupBox1.Controls.Add(this.subNo);
			this.groupBox1.Controls.Add(this.subSi);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.btnIniciar);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.txtRuta);
			this.groupBox1.Controls.Add(this.btnBuscar);
			this.groupBox1.Location = new System.Drawing.Point(20, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(728, 193);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "FileSystemWatcher";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(628, 148);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(88, 32);
			this.button2.TabIndex = 18;
			this.button2.Text = "Procces";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2_Click_1);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(551, 148);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(71, 32);
			this.button1.TabIndex = 17;
			this.button1.Text = "Reload";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1_Click_2);
			// 
			// txtFiltro
			// 
			this.txtFiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFiltro.Location = new System.Drawing.Point(57, 140);
			this.txtFiltro.Margin = new System.Windows.Forms.Padding(4);
			this.txtFiltro.Name = "txtFiltro";
			this.txtFiltro.Size = new System.Drawing.Size(96, 30);
			this.txtFiltro.TabIndex = 16;
			this.txtFiltro.Text = "*.*";
			this.txtFiltro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtFiltro.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
			// 
			// subNo
			// 
			this.subNo.AutoSize = true;
			this.subNo.Location = new System.Drawing.Point(384, 146);
			this.subNo.Name = "subNo";
			this.subNo.Size = new System.Drawing.Size(51, 24);
			this.subNo.TabIndex = 15;
			this.subNo.Text = "No";
			this.subNo.UseVisualStyleBackColor = true;
			// 
			// subSi
			// 
			this.subSi.AutoSize = true;
			this.subSi.Checked = true;
			this.subSi.Location = new System.Drawing.Point(324, 146);
			this.subSi.Name = "subSi";
			this.subSi.Size = new System.Drawing.Size(45, 24);
			this.subSi.TabIndex = 14;
			this.subSi.TabStop = true;
			this.subSi.Text = "Si";
			this.subSi.UseVisualStyleBackColor = true;
			this.subSi.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(172, 148);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(167, 20);
			this.label5.TabIndex = 12;
			this.label5.Text = "Incluir Subdirectorios";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(10, 144);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 20);
			this.label4.TabIndex = 11;
			this.label4.Text = "Filtro:";
			// 
			// btnIniciar
			// 
			this.btnIniciar.Location = new System.Drawing.Point(463, 150);
			this.btnIniciar.Margin = new System.Windows.Forms.Padding(4);
			this.btnIniciar.Name = "btnIniciar";
			this.btnIniciar.Size = new System.Drawing.Size(80, 30);
			this.btnIniciar.TabIndex = 10;
			this.btnIniciar.Text = "Iniciar";
			this.btnIniciar.UseVisualStyleBackColor = true;
			this.btnIniciar.Click += new System.EventHandler(this.button2_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(10, 28);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(88, 20);
			this.label3.TabIndex = 9;
			this.label3.Text = "Directorio:";
			// 
			// txtRuta
			// 
			this.txtRuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtRuta.Location = new System.Drawing.Point(105, 21);
			this.txtRuta.Margin = new System.Windows.Forms.Padding(4);
			this.txtRuta.Name = "txtRuta";
			this.txtRuta.Size = new System.Drawing.Size(437, 30);
			this.txtRuta.TabIndex = 8;
			// 
			// btnBuscar
			// 
			this.btnBuscar.Location = new System.Drawing.Point(560, 18);
			this.btnBuscar.Margin = new System.Windows.Forms.Padding(4);
			this.btnBuscar.Name = "btnBuscar";
			this.btnBuscar.Size = new System.Drawing.Size(96, 30);
			this.btnBuscar.TabIndex = 7;
			this.btnBuscar.Text = "Buscar...";
			this.btnBuscar.UseVisualStyleBackColor = true;
			this.btnBuscar.Click += new System.EventHandler(this.button1_Click_1);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// webBrowser1
			// 
			this.webBrowser1.Location = new System.Drawing.Point(754, 12);
			this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.Size = new System.Drawing.Size(603, 429);
			this.webBrowser1.TabIndex = 8;
			this.webBrowser1.Url = new System.Uri("http://www.google.com", System.UriKind.Absolute);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(583, 227);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(93, 38);
			this.button3.TabIndex = 9;
			this.button3.Text = "Clear All";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3_Click);
			// 
			// urlTexbox
			// 
			this.urlTexbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.urlTexbox.Location = new System.Drawing.Point(105, 71);
			this.urlTexbox.Margin = new System.Windows.Forms.Padding(4);
			this.urlTexbox.Name = "urlTexbox";
			this.urlTexbox.Size = new System.Drawing.Size(437, 30);
			this.urlTexbox.TabIndex = 19;
			this.urlTexbox.Text = "http://neapco.transferspc.com/manage/TestingUpload";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(50, 78);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(48, 20);
			this.label7.TabIndex = 21;
			this.label7.Text = "URL:";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1463, 486);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.webBrowser1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.lbxCambios);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lbxArchivos);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "Form1";
			this.Text = "Transfer File Whatcher";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxArchivos;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton subSi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRuta;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lbxCambios;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.RadioButton subNo;
        private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.WebBrowser webBrowser1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox urlTexbox;
	}
}

